create table applyPassport ( 
passportId INT AUTO_INCREMENT,
userName varchar(30) not null,
--city varchar(30) not null, state varchar(30) not null, country varchar(30) not null, zipCode varchar(30) not null,
passportOffice varchar(30) not null,
typeOfService varchar(30) not null,
bookletType varchar(30) not null,
passportNo varchar(30) not null,
issueDate timestamp default current_Timestamp,
expiryDate timestamp default CURRENT_TIMESTAMP,
Primary Key(passportId) 
)

create table applyVisa ( 
	visaId INT AUTO_INCREMENT,
	passportNo varchar(30) not null,
	userName varchar(30) not null,
	country varchar(30) not null,
	visaType varchar(30) not null,
	occupation varchar(30) not null,
	visaNo varchar(30) not null,
	status varchar(30) not null default 'Active',
	issueDate timestamp default current_Timestamp,
	expiryDate timestamp DEFAULT CURRENT_TIMESTAMP,
	primary key(visaId) )

create table customer (
id INT AUTO_INCREMENT,
FirstName varchar(30) not NULL,
LastName varchar(30) NULL,
userName varchar(30) NOT NULL,
password varchar(30) NOT NULL,
Gender varchar(10) NOT NULL,
DateOfBirth Date NOT NULL,
Qualification varchar(20) NOT NULL,
Mobile varchar(14) NOT NULL,
Email varchar(30) NOT NULL,
Address1 varchar(20) NOT NULL,
Address2 varchar(30) NULL,
City varchar(30) NOT NULL,
State varchar(30) NOT NULL,
ZipCode varchar(10) NOT NULL,
Country varchar(20) NOT NULL,
HintQuestion varchar(30) NOT NULL,
HintAnswer varchar(30) NOT NULL,
Primary Key(id) )

create table visaCancel(
	cancelvisaid INT AUTO_INCREMENT,
	passportNo VARCHAR(30) NULL,
	visaNo varchar(30) NULL,
	Primary Key(cancelvisaid)
);
create table renewPassport(
	ID INT AUTO_INCREMENT,
	passportNO VARCHAR(30) NULL,
	RenewalReason VARCHAR(255) NULL,
	RenewDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	Primary Key(ID)
);
