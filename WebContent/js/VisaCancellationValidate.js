function doValidate() {

	var error="";
	
	var visaNo=frmVisaCancellation.visaNo.value;
	var patt3 =/^[A-Za-z]{2}[0-9]{4}[A-Za-z][0-9]{3}/;	
	
	if(!(visaNo.match(patt3))){
		error+="Enter Valid Visa Number\n";
		document.getElementById("errvisaNo").innerHTML="Enter valid visa number";
	}	
	
	if(error!=""){
		
		return false;
	}
	
}