function doValidate() {
	
	var error="";
	var p1=frmRegister.password.value;
	var p2=frmRegister.retypePassword.value;
	
	if(p1!=p2 ){
	
		error+="Password Mismatch\n";
		document.getElementById("errRePassword").innerHTML="Password Mismatch";
		
	}
	
	/*if(p1.length<8)
		{
		error+="Password too short\n";
		document.getElementById("errPassword").innerHTML="Password is too short";
		}*/
	
	
	var letters = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;  
	if(!(p1.match(letters)))
		{
			error+="Password too short\n";
			document.getElementById("errPassword").innerHTML="Password should contain atleast \n 1 special character,1 capital letter and 1 number";
			
		} 
	
	var firstn=frmRegister.firstName.value;
	if(!firstn.match(/^[A-Za-z]+$/)){
		error+="Enter Valid First Name\n";
		document.getElementById("errfirstName").innerHTML="Please use alphabets";
	}
	
	var lastn=frmRegister.lastName.value;
	if(!lastn.match(/^[A-Za-z]+$/)){
		error+="Enter Valid Last Name\n";
		document.getElementById("errlastName").innerHTML="Please use alphabets";
	}
	
	var usern=frmRegister.userName.value;
	var patt3 =/^[A-Za-z0-9]+$/i;
	if(!usern.match(patt3)){
		error+="Enter Valid User Name\n";
		document.getElementById("erruserName").innerHTML="User Name should be alphanumeric only";
	}
	
	var mob=frmRegister.mobile.value;
	var patt1 =/^\d{10}$/;
	if(!mob.match(patt1)){
		error+="Enter Valid Mobile Number\n";
		document.getElementById("errMobileNumber").innerHTML="Enter 10 digit Mobile Number";
	}
	
	var email=frmRegister.Email.value;
	 if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
	  
		 error+="You have entered an invalid email address!\n";
		 document.getElementById("errEmail").innerHTML="Enter Valid Email";
	}
	 var mob=frmRegister.zipcode.value;
		var patt2 =/^\d{6}$/;
		if(!mob.match(patt2)){
			error+="Enter Valid Zip Code Number\n";
			 document.getElementById("errZip").innerHTML="Enter 6 digit ZipCode";
		}
	 
	var dob=frmRegister.DOB.value;
	var dt=new Date(dob);
	var today=new Date();
	
	if(dt > today){
		error+="Date Must be less than or equal to Today Date\n";
	}
	if(error!=""){
		//alert(error);
		return false;
	}

	
}