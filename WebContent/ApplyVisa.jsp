<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Apply Visa</title>
<script type="text/JavaScript" language="JavaScript" src="js/ApplyVisaValidate.js"></script>
<!--  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
 --><link rel="stylesheet" type="text/css" href="css/NewFile.css" />
</head>

<body id="id2">

<%@ page import="com.cts.beans.Customer" %>
<%@ page import="com.cts.dao.CustomerDao" %>
<%
Customer customer=null;
%>

<% if(request.getSession().getAttribute("customer")==null){ 
    	request.getSession().setAttribute("message","please login to access the Page ");
    	response.sendRedirect("/login.jsp");
 		return;   	
    }else{
    
    	customer=(Customer)request.getSession().getAttribute("customer");
    	CustomerDao dao=new CustomerDao();
    	
    	customer=(Customer)dao.getUser(customer.getUserName());
    	session.setAttribute("customer", customer);
        
    }%>    
<jsp:include page="navbar.jsp" />
	<div class="container wrapper">
		         <div id="top">
	
			<table  width="100%">
				<tr height="20px">
				<td width="15%">
				<img src="Images/logo.JPG" height="120" width="250"/>
				</td>
				
				
				<td width="85%" style="text-align: center;padding-right: 10px;">
				
				<h1>
				
				<strong>Passport And Visa Seva</strong>
				
			</h1>
			<p>
				<marquee>
					<sub>Passport and Visa Division</sub>
				</marquee>
			</p>
			
				</td>
			
				</tr>
			</table>
				</div>
	</div>

<div  margin-left: 400px; margin-right: 400px;">
 <form name="frmApplyVisa" action="ApplyVisaServer" method="post" onsubmit="return doValidate()">
  
  <h2 align="center"><u>Visa Application Form</u></h2>
  <h4 align="right">Fields marked with (<font color="red">*</font>) are mandatory</h4>
    
    <table align="center" border="0">
	<tbody>
		<tr height="30px">
			<td width="30%"></td>
			<td width="70%" align="center"></td>
		</tr>
		<tr height="30px">
			<td align="center"><b>Passport Number<font color="red">*</font></b></td>
			<td align="center"><input name="passportNumber" type="Text" placeholder="Enter Passport Number" value="<%=customer.getPassportNumber()%>" readonly  required>
			<span id="errpassportNo" style="color:red;"></span></td>
		</tr>
		<tr height="30px">
			<td align="center"><b>User Name<font color="red">*</font></b></td>
			<td align="center"><input name="userName" type="Text" placeholder="Enter User Name" value="<%=customer.getUserName()%>" readonly required>
			<span id="erruserName" style="color:red;"></span></td>
		</tr>
		<tr  height="30px">
			<td align="center"><b>Country<font color="red">*</font></b></td>
			<td>
			<select name="country" required>
					<option value="" disabled selected>------------Select Country------------</option>
				<option value="India">India</option>	
				<option value="Afghanistan">Afghanistan</option>									
				<option value="Albania">Albania</option>
				<option value="Algeria">Algeria</option>									
				<option value="Andorra">Andorra</option>
				<option value="Australia">Australia</option>	
				<option value="Norway">Norway</option>
				<option value="Oman">Oman</option>
				<option value="Pakistan">Pakistan</option>
				<option value="Palau">Palau</option>
				<option value="Slovakia">Slovakia</option>
				<option value="Slovenia">Slovenia</option>
				<option value="Solomon Islands">Solomon Islands</option>
				<option value="Somalia">Somalia</option>
				<option value="Tunisia">Tunisia</option>
				<option value="Turkey">Turkey</option>
				<option value="Turkmenistan">Turkmenistan</option>
				<option value="Ukraine">Ukraine</option>
				<option value="United Arab Emirates">United Arab Emirates</option>
				<option value="United Kingdom">United Kingdom</option>
				<option value="United States">United States</option>			
		</select>							
			</td>			
		</tr>
		
		
		<tr  height="30px">
			<td align="center"><b>Visa Type<font color="red">*</font></b></td>
			<td>
			<select name="visaType" required>
					<option value="" disabled selected>------------Select Type of Visa------------</option>
				<option value="Business">Business</option>	
				<option value="Tourist">Tourist</option>
				<option value="Transit">Transit</option>									
				<option value="Student">Student</option>										
				
		</select>							
			</td>			
		</tr>
		
		
		
		
		<tr  height="30px">
			<td align="center"><b>Occupation<font color="red">*</font></b></td>
			<td align="center"><input name="occupation" type="Text" placeholder="Enter Occupation" required>
			<span id="erroccupation" style="color:red;"></span></td>
		</tr>
		
		
	<tr  height="40px">

			<td align="center"><input type="reset" value="Reset">
			</td>
			<td align="center"><input type="submit" value="Submit">
			</td> 
	</tbody>
</table>
  </form>
</div>
</body>
</html>