<!DOCTYPE html>
<html>
<head>
<!-- <script src="js/CustomerValidate.js"></script> -->
<meta charset="ISO-8859-1">
<title>Passport Renewal</title>
<!--  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
 -->
 <script type="text/JavaScript" language="JavaScript" src="js/RenewPassportValidate.js"></script>
<link rel="stylesheet" type="text/css" href="css/NewFile.css" />
</head>

<body id="id2">
<%@ page import="com.cts.beans.Customer" %>
<%@ page import="com.cts.dao.CustomerDao" %>
<%
Customer customer=null;
%>

<% if(request.getSession().getAttribute("customer")==null){ 
    	request.getSession().setAttribute("message","please login to access the Page ");
    	response.sendRedirect("/login.jsp");
 		return;   	
    }else{
    
    	customer=(Customer)request.getSession().getAttribute("customer");
    	CustomerDao dao=new CustomerDao();
    	
    	customer=(Customer)dao.getUser(customer.getUserName());
    	session.setAttribute("customer", customer);
        
    }%>    

<jsp:include page="navbar.jsp" />
	<div class="container wrapper">
		         <div id="top">
	
			<table  width="100%">
				<tr height="20px">
				<td width="15%">
				<img src="Images/logo.JPG" height="120" width="250"/>
				</td>				
				<td width="85%" style="text-align: center;padding-right: 10px;">				
				<h1>				
				<strong>Passport And Visa Seva</strong>				
			</h1>
			<p>
				<marquee>
					<sub> Passport and Visa Division</sub>
				</marquee>
			</p>			
				</td>			
				</tr>
			</table>
				</div>
	</div>

<div  margin-left: 400px; margin-right: 400px;">
 <form name="frmRenewPassport" action="RenewPassportServer" method="post" onsubmit="return doValidate()">
 
  <h2 align="center"><u>Passport Renewal Application Form</u></h2>
  <h4 align="right">Fields marked with (<font color="red">*</font>) are mandatory</h4>
    
    <table align="center" border="0">
	<tbody>
		<tr height="30px">
			<td width="30%"></td>
			<td width="70%" align="center"></td>
		</tr>
		
		
		<tr  height="30px">
			<td align="center"><b>Passport No.<font color="red">*</font></b></td>
			<td align="center"><input name="passportNo" type="Text" placeholder="Enter Passport Number" value="<%=customer.getPassportNumber() %>" readonly required>
			<span id="errpassportNo" style="color:red;"></span></td>
		</tr>
		
		<tr  height="30px">
			<td align="center"><b>Reason for Renewal<font color="red">*</font></b></td>
			<td>
			<select name="reasonForRenewal" required>		
				<option value="" disabled selected>--------Select Reason For Renewal--------</option>
			<option value="passportExpired">Passport Expired</option> 
			<option value="passportLost">Passport Lost</option>
			<option value="passportDamaged">Passport Damaged</option>
		</select>							
			</td>			
		</tr> 		
		
		<!-- <tr  height="30px">
			<td align="center"><b>Type Of Service<font color="red">*</font></b></td>
			<td align="center"><input type="radio" name="typeOfService" value="ordinary" checked> Ordinary
			<input type="radio" name="typeOfService" value="tatkal">Tatkal
							
		</tr> 	
		<tr  height="30px">
			<td align="center"><b>Booklet Type<font color="red">*</font></b></td>
			<td align="center"><input type="radio" name="bookletType" value="30 pages" checked> 30 Pages
			<input type="radio" name="bookletType" value="60 pages">60 Pages
			</td>
		</tr>		 -->
			
		<tr  height="40px">
			<td align="center"><input type="reset" value="Reset">
			</td>
			<td align="center"><input type="submit" value="Submit">
			</td>
		
	</tbody>
</table>
 <span id="error"></span> 
  </form>
</div>
</body>
</html>