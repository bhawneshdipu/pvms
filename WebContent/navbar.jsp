
  
    
<body id="id2" >
    
    
<%@ page import="com.cts.beans.Customer" %>
<%@ page import="com.cts.dao.CustomerDao" %>
<%
Customer customer=null;
%>

<% if(request.getSession().getAttribute("customer")==null){ 
    	request.getSession().setAttribute("message","please login to access the Page ");
    	response.sendRedirect("/login.jsp");
 		return;   	
    }else{
    
    	customer=(Customer)request.getSession().getAttribute("customer");
    	CustomerDao dao=new CustomerDao();
    	
    	customer=(Customer)dao.getUser(customer.getUserName());
    	session.setAttribute("customer", customer);
        
    }%>    


<link rel="stylesheet" href="/css/3.3.7/bootstrap.min.css">

<!-- jQuery library -->
<script src="/js/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="/js/3.3.7/bootstrap.min.js"></script> 
<script src="/js/sweetalert.min.js"></script> 
   
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">PVMS</a>
    </div>
    <% if(request.getSession().getAttribute("customer")!=null){ %>
    <%
    	customer=(Customer)request.getSession().getAttribute("customer");
    %>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/loginsuccess.jsp">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a class="" href="/ApplyPassport.jsp">Apply Passport</a></li>
      <li><a class="" href="/ApplyVisa.jsp">Apply Visa</a></li>
		<li><a class="" href="/RenewPassport.jsp">Re-issue Passport</a></li>
	<li><a class="" href="/VisaCancellation.jsp">Visa Cancellation</a></li>
                  
      <li><a href="#"><span class="glyphicon glyphicon-user"></span><%=customer.getEmail() %></a></li>
      <li><a onclick="alertLogout();"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
    
    <%}else{ %>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/home.html">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/Register.html"><span class="glyphicon glyphicon-user"></span> Register</a></li>
      <li><a href="/login.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
    <%} %>
  </div>
</nav>
<script>
	function alertLogout(){
		 swal({
		      title: "Are you sure?",
		      text: "You will not be able to use PSVM Portal !!!",
		      icon: "warning",
		      buttons: [
		        'No, cancel it!',
		        'Yes, I am sure!'
		      ],
		      dangerMode: true,
		    }).then(function(isConfirm) {
		      if (isConfirm) {
		        swal({
		          title: 'Logout Success!',
		          text: 'You Are Successfully Logout From the System!',
		          icon: 'success'
		        }).then(function() {
		        	window.location.href = "/LogoutServlet";
		        });
		      } else {
		        swal("Cancelled", "You Are still Logged In :)", "error");
		      }
		    });
	}
</script>