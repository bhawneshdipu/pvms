<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Date"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.cts.utilities.DBUtility"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		Connection con=DBUtility.getConnection();
		PreparedStatement pst=con.prepareStatement("SELECT DATE_ADD(curdate(), INTERVAL 10 YEAR) ny");
		ResultSet rs=pst.executeQuery();
		rs.next();
		Date dt=rs.getDate("ny");
		pst=con.prepareStatement("Update DummyD set updDate=?"); 
		pst.setDate(1,dt);
		pst.executeUpdate();
		out.println("*** Updated ***");
	%>
</body>
</html>