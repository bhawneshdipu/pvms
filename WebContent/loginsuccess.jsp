<html>
<head>
    <title>Page Title</title>
<link rel="stylesheet" href="/css/3.3.7/bootstrap.min.css">

<!-- jQuery library -->
<script src="/js/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="/js/3.3.7/bootstrap.min.js"></script> 
<script src="/js/sweetalert.min.js"></script> 
      <link rel="stylesheet" type="text/css" href="/css/NewFile.css" /> 

</head>

<body id="id2">
<%@ page import="com.cts.beans.Customer" %>
<%@ page import="com.cts.dao.CustomerDao" %>
<%
Customer customer=null;
%>

<% if(request.getSession().getAttribute("customer")==null){ 
    	request.getSession().setAttribute("message","please login to access the Page ");
    	response.sendRedirect("/login.jsp");
 		return;   	
    }else{
    
    	customer=(Customer)request.getSession().getAttribute("customer");
    	CustomerDao dao=new CustomerDao();
    	
    	customer=(Customer)dao.getUser(customer.getUserName());
    	session.setAttribute("customer", customer);
        
    }%>    


<jsp:include page="navbar.jsp" />


    <div class="container wrapper">
		         <div id="top">
	
			<table  width="100%">
				<tr height="20px">
				<td width="15%">
				<img src="Images/logo.JPG" height="120" width="250"/>
				</td>
				
				
				<td width="85%" style="text-align: center;padding-right: 10px;">
				
				<h1>
				
				<strong>Passport And Visa Seva</strong>
				
			</h1>
			<p>
				<marquee>
					<sub>Passport and Visa Division</sub>
				</marquee>
			</p>
			
				</td>
			
				</tr>
			</table>
				</div>
	</div>
<div class="container">
    <div class="col-md-8 col-sm-offset-2">
           <!--  <h1 class='text-center'> Welcome To Passport Visa Seva</h1> -->
            <!--<div class="row btn-group">
                <div class='col-sm-3'>
                        <a class="btn btn-lg btn-primary" href="./login.html">Login</a>

                </div>
                <div class='col-sm-3'>

                <a class="btn btn-lg btn-info" href="./index.html">Home</a>
            </div>

                <div class='col-sm-3'>

                <a class="btn btn-lg btn-success" href="./cancellationvisa.html">Visa Cancellation</a>
            </div>
                 
        </div>-->
        <div class='clearfix'>&nbsp;</div>

          
            
      
          <%if((customer.getPassportNumber()==null || customer.getPassportNumber().equals("") ||customer.getPassportNumber().length()==0 )) {%>
           
			<a class="btn btn-lg btn-info btn-block" href="/ApplyPassport.jsp">Apply Passport</a>
        <%} %>
          <%if(!(customer.getPassportNumber()==null || customer.getPassportNumber().equals("") ||customer.getPassportNumber().length()==0 )) {%>
            <a class="btn btn-lg btn-primary btn-block" href="/RenewPassport.jsp">Re-issue Passport</a>
			<%} %>
     	     <%if((customer.getVisaNumber()==null || customer.getVisaNumber().equals("") ||customer.getVisaNumber().length()==0 )) {%>
     		<a class="btn btn-lg btn-primary btn-block" href="/ApplyVisa.jsp">Apply Visa</a>
		 <%} %>  	    
		     <%if(!(customer.getVisaNumber()==null || customer.getVisaNumber().equals("") ||customer.getVisaNumber().length()==0 )) {%>
     
				<a class="btn btn-lg btn-success btn-block" href="/VisaCancellation.jsp">Visa Cancellation</a>
            <%} %>
             
	
            
    </div>
</div>
<div class="container">
 <!-- <span id="result2" style="font-size:25px; color: White;visibility:hidden"><b>Successfully Applied...</b></span> -->

   </div>
</body>


<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>

<script type="text/javascript" src="/js/bootstrap.min.js"></script>

</html>