<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">


<title>Apply Passport</title>
<script type="text/JavaScript" language="JavaScript" src="js/ApplyPassportValidate.js"></script>
<script>
/* function show() {
	var name = '<%= session.getAttribute("user") %>';
	alert(name);
} */



function goBack() {
    window.history.back();
}


</script>
<link rel="stylesheet" type="text/css" href="css/NewFile.css" />
</head>
<button onclick="goBack()"><--</button>
<%@ page import="com.cts.beans.Customer" %>
<%@ page import="com.cts.dao.CustomerDao" %>
<%
Customer customer=null;
%>

<% if(request.getSession().getAttribute("customer")==null){ 
    	request.getSession().setAttribute("message","please login to access the Page ");
    	response.sendRedirect("/login.jsp");
 		return;   	
    }else{
    
    	customer=(Customer)request.getSession().getAttribute("customer");
    	CustomerDao dao=new CustomerDao();
    	
    	customer=(Customer)dao.getUser(customer.getUserName());
    	session.setAttribute("customer", customer);
        
    }%>    
<jsp:include page="navbar.jsp" />

	<div class="container wrapper">
		         <div id="top">
	
			<table  width="100%">
				<tr height="20px">
				<td width="15%">
				<img src="Images/logo.JPG" height="120" width="250"/>
				</td>
				<td width="85%" style="text-align: center;padding-right: 10px;">
				<h1>
				<strong>Passport And Visa Seva</strong>
				
			</h1>
			<p>
				<marquee>
					<sub>Passport and Visa Division</sub>
				</marquee>
			</p>
			
				</td>
			
				</tr>
			</table>
				</div>
	</div>

<div  margin-left: 400px; margin-right: 400px;">

 <form name="frmApplyPassport" action="ApplyPassportServer" method="post" onsubmit="return doValidate()"> 
  
  
  
  <h2 align="center"><u>Passport Application Form</u></h2>
    <h4 align="right">Fields marked with (<font color="red">*</font>) are mandatory</h4>
    <table align="center" border="0">
	<tbody>
		<tr height="30px">
			<td width="30%"></td>
			<td width="70%" align="center"></td>
		</tr>
		<tr height="30px">
			<td align="center"><b>User Name<font color="red">*</font></b></td>
			<td align="center"><input name="userName" type="Text" placeholder="Enter User Name" required value="<%=customer.getUserName()%>" readonly>
			<span id="erruserName" style="color:red;"></span></td>
		</tr>
		
		<tr  height="30px">
			<td align="center"><b>Passport Office<font color="red">*</font></b></td>
			<td>
			<select name="passportoffice" required>		
				<option value="" disabled selected>------------Select City------------</option>
			<option value="Ahmedabad">Ahmedabad</option> 
			<option value="Bengaluru">Bengaluru</option>
			<option value="Chandigarh">Chandigarh</option>
			<option value="Chennai">Chennai</option>
			<option value="Delhi">Delhi</option>
			<option value="Gurgaon">Gurgaon</option>
			<option value="Hyderabad">Hyderabad</option>
			<option value="Kolkatta">Kolkatta</option>
			<option value="Mumbai">Mumbai</option>
			<option value="Noida">Noida</option>
			<option value="Pune">Pune</option>
			<option value="Anantapur">Anantapur</option>
			<option value="Guntakal">Guntakal</option>
			<option value="Guntur">Guntur</option>										
			<option value="kakinada">kakinada</option>
			<option value="kurnool">kurnool</option>
			<option value="Nellore">Nellore</option>
			<option value="Nizamabad">Nizamabad</option>
			<option value="Rajahmundry">Rajahmundry</option>
			<option value="Tirupati">Tirupati</option>
			<option value="Vijayawada">Vijayawada</option>
			<option value="Visakhapatnam">Visakhapatnam</option>
			<option value="Warangal">Warangal</option>
						
		</select>							
			</td>			
		</tr> 
		
		<tr  height="30px">
			<td align="center"><b>Type Of Service<font color="red">*</font></b></td>
			<td align="center"><input type="radio" name="typeOfService" value="ordinary" checked> Ordinary
			<input type="radio" name="typeOfService" value="tatkal">Tatkal
							
		</tr> 	
		<tr  height="30px">
			<td align="center"><b>Booklet Type<font color="red">*</font></b></td>
			<td align="center"><input type="radio" name="bookletType" value="30 pages" checked> 30 Pages
			<input type="radio" name="bookletType" value="60 pages">60 Pages
			</td>
		</tr>
		
		
	<tr  height="40px">

			<td align="center"><input type="reset" value="Reset">
			</td>
			<td align="center"><input type="submit" value="Submit">
			</td> 
	</tbody>
</table>
  </form>
</div>
</body>
</html>