package com.cts.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DBUtility {
    public static Connection getConnection()
    {
    	ResourceBundle rb=ResourceBundle.getBundle("db");
    	Connection con=null;
    	
    	String driver=rb.getString("driver");
    	String url=rb.getString("url");
    	String username=rb.getString("username");
    	String password=rb.getString("password");
    	
    	
    	try {
			Class.forName(driver);
			con= DriverManager.getConnection(url,username,password);
			//System.out.println("suceess");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
             return con;
    	
    }
    
    public static void main(String[] args) {
		getConnection();
	}
}
