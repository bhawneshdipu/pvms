package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cts.beans.ApplyVisa;
import com.cts.beans.Customer;
import com.cts.beans.VisaCancel;
import com.cts.dao.VisaCancelDao;

/**
 * Servlet implementation class VisaCancelServer
 */
public class VisaCancelServer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VisaCancelServer() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/loginsuccess.jsp");
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	/*	VisaCancel objVisaCancel=new VisaCancel();		
		objVisaCancel.setVisaNo(request.getParameter("visaNo"));		
        VisaCancelDao objVisaCancelDao=new VisaCancelDao();*/
        PrintWriter out=response.getWriter();
        String visaNo=request.getParameter("visaNo");
        VisaCancelDao dao=new VisaCancelDao();
        String msg=dao.visaExpire(visaNo);
        
	HttpSession session=request.getSession(true);
		
		if(msg.equals("Visa Successfully Cancelled"))
		{
//			session.setAttribute("dao","Visa Successfully Cancelled");
			RequestDispatcher rd=request.getRequestDispatcher("VisaCancelSuccess.jsp");  
			rd.include(request, response);  
			 out.println("<script>document.getElementById('result2').style.visibility = 'visible';</script>");
		}
		else
		{
			session.setAttribute("dao",msg);
			RequestDispatcher rd=request.getRequestDispatcher("VisaCancelFail.jsp");  
			rd.include(request, response);  
			 out.println("<script>document.getElementById('result3').style.visibility = 'visible';</script>");
		}
	}

}
