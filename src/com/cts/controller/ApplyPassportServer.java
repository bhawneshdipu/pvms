package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cts.beans.ApplyPassport;
import com.cts.dao.ApplyPassportDao;


/**
 * Servlet implementation class ApplyPassportServer
 */
public class ApplyPassportServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplyPassportServer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/loginsuccess.jsp");
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 ApplyPassport objApplyPassport =new ApplyPassport();		

	       objApplyPassport.setUserName(request.getParameter("userName"));
	       objApplyPassport.setPassportOffice(request.getParameter("passportoffice"));
	       objApplyPassport.setBookletType(request.getParameter("bookletType"));
	       objApplyPassport.setTypeOfService(request.getParameter("typeOfService"));
	       
	       	ApplyPassportDao objApplyPassportDao =new ApplyPassportDao ();
			PrintWriter out=response.getWriter();
			String b=objApplyPassportDao.applyPassport(objApplyPassport);			
		
			HttpSession session=request.getSession(true);
			session.setAttribute("passportNo",objApplyPassport.getPassportNo()); 
			ApplyPassport dao=new ApplyPassportDao().searchPassport(objApplyPassport.getPassportNo());
			session.setAttribute("dao",dao);
		
			session.setAttribute("message",b);
			if(b.equals("Successfully Applied...")){  				
				
					RequestDispatcher rd=request.getRequestDispatcher("ApplyPassportSuccess.jsp");  
					rd.include(request, response);  
					out.println("<script>document.getElementById('result2').style.visibility = 'visible';</script>");
				}  
				else
				{  
					RequestDispatcher disp=request.getRequestDispatcher("ApplyPassportFail.jsp");
					disp.include(request, response);					
					out.println("<script>document.getElementById('result3').style.visibility = 'visible';</script>");
				}
	}

}
