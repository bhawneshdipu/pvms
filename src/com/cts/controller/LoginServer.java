package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cts.beans.Customer;
import com.cts.dao.CustomerDao;

/**
 * Servlet implementation class LoginServer
 */
public class LoginServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user=request.getParameter("user");
		String pass=request.getParameter("pass");
		HttpSession session=request.getSession(true); 
		CustomerDao objCustomerDao =new CustomerDao ();
		PrintWriter out = response.getWriter();
		//     System.out.println("pass in servlet=="+objCustomerDao.validateLogin(user));
		if(pass.equals(objCustomerDao.validateLogin(user))){  
			//out.print("<mark><marquee>Successfully Logged in....</marquee></mark>");  
			//commented
			//RequestDispatcher rd=request.getRequestDispatcher("loginsuccess.html");  
			//rd.include(request, response);
			//added
			Customer customer=objCustomerDao.getUser(user);
			session.setAttribute("user",user);
			session.setAttribute("customer",customer);
			request.getRequestDispatcher("/loginsuccess.jsp")
			.forward(request, response);
			 return;
		}  
		else{  
			//
			//out.print("Sorry UserName or Password Error!");  
			out.println("<script>document.getElementById('result').style.visibility = 'visible';</script>");
			//added
			request.getSession().setAttribute("message", "Invalid Username and Password! Please try again");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
			return;
			//commented
			//RequestDispatcher rd=request.getRequestDispatcher("login.jsp");  
			//rd.include(request, response);  
			
		}
	}

}
