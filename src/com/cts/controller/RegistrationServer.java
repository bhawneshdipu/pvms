package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cts.beans.Customer;
import com.cts.dao.CustomerDao;

/**
 * Servlet implementation class RegistrationServer
 */
public class RegistrationServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/loginsuccess.jsp");
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); // your template here
		java.util.Date dateStr=null;
		try {
			dateStr = formatter.parse(request.getParameter("DOB"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		java.sql.Date dateDB = new java.sql.Date(dateStr.getTime());
		Customer objCustomer=new Customer();
	   objCustomer.setFirstName(request.getParameter("firstName"));
	   objCustomer.setLastName(request.getParameter("lastName"));
	   objCustomer.setUserName(request.getParameter("userName"));
	   objCustomer.setPassWord(request.getParameter("password"));
	   objCustomer.setGender(request.getParameter("gender"));
	  
		objCustomer.setDateOfBirth(dateDB);

	   objCustomer.setMobile(request.getParameter("mobile"));
	   objCustomer.setEmail(request.getParameter("Email"));
	   objCustomer.setQualification(request.getParameter("qualification"));
	   objCustomer.setAddress1(request.getParameter("address1"));
	   objCustomer.setAddress2(request.getParameter("address2"));
	   objCustomer.setCity(request.getParameter("city"));
	   objCustomer.setState(request.getParameter("state"));
	   objCustomer.setZipcode(request.getParameter("zipcode"));
	   objCustomer.setCountry(request.getParameter("country"));
	   objCustomer.setHintQuestion(request.getParameter("hintQuestion"));
	   objCustomer.setHintAnswer(request.getParameter("hintAnswer"));
	   
	  CustomerDao objCustomerDao =new CustomerDao ();
		PrintWriter out=response.getWriter();
		String b=objCustomerDao.addCustomer(objCustomer);
		
		 if(b.equals("Record Inserted...")){  
//			out.print("<b>Successfully Registered You can login in....<b>");  
			RequestDispatcher rd=request.getRequestDispatcher("login.html");  
			rd.include(request, response);  
			 out.println("<script>document.getElementById('result2').style.visibility = 'visible';</script>");
		}  
		else
		{  
//			out.print("<b>"+b+"</b>");  
			RequestDispatcher rd=request.getRequestDispatcher("home.html");  
			rd.include(request, response);  
			 out.println("<script>document.getElementById('result3').style.visibility = 'visible';</script>");
		}
	}

}
