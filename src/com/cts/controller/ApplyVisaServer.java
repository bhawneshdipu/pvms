package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cts.beans.ApplyPassport;
import com.cts.beans.ApplyVisa;
import com.cts.dao.ApplyPassportDao;
import com.cts.dao.ApplyVisaDao;

/**
 * Servlet implementation class ApplyVisaServer
 */
public class ApplyVisaServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplyVisaServer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/loginsuccess.jsp");
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		 ApplyVisa objApplyVisa =new ApplyVisa();

		 objApplyVisa.setPassportNo(request.getParameter("passportNumber"));
	       
	       
		 objApplyVisa.setUserName(request.getParameter("userName"));
		 objApplyVisa.setCountry(request.getParameter("country"));
		 objApplyVisa.setVisaType(request.getParameter("visaType"));
		 objApplyVisa.setOccupation(request.getParameter("occupation"));
		

	      ApplyVisaDao objApplyVisaDao =new ApplyVisaDao();
			PrintWriter out=response.getWriter();
			String b=objApplyVisaDao.applyVisa(objApplyVisa);  

			HttpSession session=request.getSession(true);
			session.setAttribute("visaNo",objApplyVisa.getVisaNo()); 
			ApplyVisa dao=new ApplyVisaDao().searchVisa(objApplyVisa.getVisaNo(),objApplyVisa.getVisaType());
			session.setAttribute("dao",dao);

			session.setAttribute("message",b);
			if(b.equals("Successfully Applied for visa...")){  
				
				
					RequestDispatcher rd=request.getRequestDispatcher("ApplyVisaSuccess.jsp");  
					rd.include(request, response);  
					 out.println("<script>document.getElementById('result2').style.visibility = 'visible';</script>");
				}  
				else
				{  
					
					RequestDispatcher rd=request.getRequestDispatcher("ApplyVisaFail.jsp");  
					rd.include(request, response);
					out.println("<script>document.getElementById('result3').style.visibility = 'visible';</script>");
				}
			
	
	
	}

}
