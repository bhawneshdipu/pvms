package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cts.beans.Customer;
import com.cts.dao.CustomerDao;

/**
 * Servlet implementation class LoginServer
 */
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getSession().getAttribute("customer")!=null) {
			request.getSession().removeAttribute("customer");
			request.getSession().removeAttribute("user");				
		}
		request.getSession().setAttribute("message", "Successfully logout!");
		request.getRequestDispatcher("/login.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			if(request.getSession().getAttribute("customer")!=null) {
				request.getSession().removeAttribute("customer");
				request.getSession().removeAttribute("user");				
			}
			request.getSession().setAttribute("message", "Successfully logout!");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
				
				return;
		
	}

}
