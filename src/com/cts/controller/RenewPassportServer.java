package com.cts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cts.beans.ApplyPassport;
import com.cts.beans.RenewPassport;
import com.cts.dao.RenewPassportDao;

/**
 * Servlet implementation class RenewPassportServlet
 */
public class RenewPassportServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RenewPassportServer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/loginsuccess.jsp");
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       RenewPassport objRenewPassport =new RenewPassport();
		String passportNo=request.getParameter("passportNo");
		objRenewPassport.setPassportNo(passportNo);
		objRenewPassport.setReasonForRenewal(request.getParameter("reasonForRenewal"));

	 	RenewPassportDao objRenewPassportDao =new RenewPassportDao ();
	 	ApplyPassport objApplyPassport=new ApplyPassport();
		PrintWriter out=response.getWriter();
		
		if(request.getParameter("reasonForRenewal").equals("passportExpired"))
		{
			objApplyPassport=objRenewPassportDao.renewPassportExpiry(passportNo);
		}
		if(request.getParameter("reasonForRenewal").equals("passportLost"))
		{
			objApplyPassport=objRenewPassportDao.checkLostPassPort(passportNo);
		}
		if(request.getParameter("reasonForRenewal").equals("passportDamaged"))
		{
			objApplyPassport=objRenewPassportDao.checkDamagedPassport(passportNo);
		}
//		System.out.println("server data="+objApplyPassport.getExpiryDate());
		HttpSession session=request.getSession(true);
		
		if(objApplyPassport!= null)
		{
			session.setAttribute("dao",objApplyPassport);
			RequestDispatcher rd=request.getRequestDispatcher("RenewPassportSuccess.jsp");  
			rd.include(request, response);  
			 out.println("<script>document.getElementById('result3').style.visibility = 'visible';</script>");
		}
		else
		{
			RequestDispatcher rd=request.getRequestDispatcher("RenewPassportFail.jsp");  
			rd.include(request, response);  
			 out.println("<script>document.getElementById('result3').style.visibility = 'visible';</script>");
		}
	}

}
