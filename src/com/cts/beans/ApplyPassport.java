package com.cts.beans;

import java.sql.Date;


public class ApplyPassport {
	
	private Date issueDate;
	private String userName;
	private Date expiryDate;
	
	private String  passportOffice;
	private String typeOfService;
	private  String bookletType;
	private String passportNo;
	
	public String getPassportOffice() {
		return passportOffice;
	}
	public void setPassportOffice(String passportOffice) {
		this.passportOffice = passportOffice;
	}
	public String getTypeOfService() {
		return typeOfService;
	}
	public void setTypeOfService(String typeOfService) {
		this.typeOfService = typeOfService;
	}
	public String getBookletType() {
		return bookletType;
	}
	public void setBookletType(String bookletType) {
		this.bookletType = bookletType;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
}
