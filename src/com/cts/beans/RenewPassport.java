package com.cts.beans;


public class RenewPassport {
	
	
	private String passportNo;	
	private String reasonForRenewal;	
	
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}	
	public String getReasonForRenewal() {
		return reasonForRenewal;
	}
	public void setReasonForRenewal(String reasonForRenewal) {
		this.reasonForRenewal = reasonForRenewal;
	}	
	
		
}
