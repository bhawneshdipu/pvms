package com.cts.beans;

public class VisaCancel {
	private String visaNo;
	
	public String getVisaNo() {
		return visaNo;
	}
	public void setVisaNo(String visaNo) {
		this.visaNo = visaNo;
	}
}