package com.cts.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import sun.java2d.pipe.SpanShapeRenderer.Simple;

//import com.cts.beans.ApplyPassport;
import com.cts.beans.Customer;
import com.cts.utilities.DBUtility;

public class CustomerDao {
	
	/*public String applyPassport(ApplyPassport objApplyPassport)
	{
		boolean flag=userExists(objApplyPassport.getUserId());
		String msg="";
		if(flag==true){
			Connection con=DBUtility.getConnection();
			String cmdApplyPassIns="Insert into applyPassport(userName,city,state,country,zipCode,passportOffice,typeOfService,bookletType) values(?,?,?,?,?,?,?,?)";
	      try {
			PreparedStatement pst=con.prepareStatement(cmdApplyPassIns);
			pst.setString(1,objApplyPassport.getUserId());
			pst.setString(2,objApplyPassport.getCity());
			pst.setString(3,objApplyPassport.getState());
			pst.setString(4,objApplyPassport.getCountry());
			pst.setString(5,objApplyPassport.getZipCode());
			pst.setString(6,objApplyPassport.getPassportOffice());
			pst.setString(7,objApplyPassport.getTypeOfService());
			pst.setString(8,objApplyPassport.getBookletType());
			int n=pst.executeUpdate();
			if(n>0){
				msg="Successfully Applied...";
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      
		}
		else {
			msg="Invalid UserID ...";
		}
		return msg;
		
		
	}
*/
	public boolean userExists(String user){
		boolean flag=false;
		String userCmd="select userName from customer where userName=?";
		Connection con=DBUtility.getConnection();
		try {
			PreparedStatement pst=con.prepareStatement(userCmd);
			pst.setString(1,user); 
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				flag=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	public String addCustomer(Customer objCustomer){
	
		
		boolean flag=userExists(objCustomer.getUserName());
		String msg="";
		if(flag==false){
			Connection con=DBUtility.getConnection();
			String cmdCustIns="Insert into customer(firstName,lastName,userName,password,gender,"
					+ "dateofbirth,mobile,email,qualification,address1,address2,city,state,zipcode,"
					+ "country,hintquestion,hintanswer) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			try {
				PreparedStatement pst=con.prepareStatement(cmdCustIns);
				pst.setString(1,objCustomer.getFirstName());
				pst.setString(2,objCustomer.getLastName());
				pst.setString(3,objCustomer.getUserName());
				pst.setString(4,objCustomer.getPassWord());
				pst.setString(5,objCustomer.getGender());
				pst.setDate(6,objCustomer.getDateOfBirth());
				pst.setString(7,objCustomer.getMobile());
				pst.setString(8,objCustomer.getEmail());
				pst.setString(9,objCustomer.getQualification());
				pst.setString(10,objCustomer.getAddress1());
				pst.setString(11,objCustomer.getAddress2());
				pst.setString(12,objCustomer.getCity());
				pst.setString(13,objCustomer.getState());
				pst.setString(14,objCustomer.getZipcode());
				pst.setString(15,objCustomer.getCountry());
				//pst.setString(16,objCustomer.getApplyType());
				pst.setString(16,objCustomer.getHintQuestion());
				pst.setString(17,objCustomer.getHintAnswer());
				int n=pst.executeUpdate();
				if(n>0){
					msg="Record Inserted...";
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			msg="UserID Already Exists...";
		}
		return msg;
	}
	
	public String validateLogin(String user)
	{
		boolean flag=userExists(user);
		String msg="";
		
		if(flag==true){
			
			Connection con=DBUtility.getConnection();
			String cmdCustLogin="Select password from customer where username=?";					
					
			try {
				PreparedStatement pst=con.prepareStatement(cmdCustLogin);
				pst.setString(1,user);
				ResultSet rs=pst.executeQuery();  
				while(rs.next()){  
					msg=rs.getString(1);
//				System.out.println("password from db=="+rs.getString(1));  
				}  
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		else
		{
			msg="Username does not exists....";
		}
		
		return msg;
	}
	
	//added
	public Customer getUser(String user)
	{
		boolean flag=userExists(user);
		String msg="";
		
		if(flag==true){
			
			Connection con=DBUtility.getConnection();
			String cmdCustLogin="select * from customer where username=?";					
			Customer usr=new Customer();
					
			try {
				PreparedStatement pst=con.prepareStatement(cmdCustLogin);
				pst.setString(1,user);
				ResultSet rs=pst.executeQuery();  
				while(rs.next()){  
					msg=rs.getString(1);
					usr.setFirstName(rs.getString(2));
					usr.setLastName(rs.getString(3));
					usr.setEmail(rs.getString(10));
					usr.setUserName(rs.getString(4));
					//return usr;
//				System.out.println("password from db=="+rs.getString(1));  
				}  
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String passportDetail="select passportNo from applyPassport where username=?";					
				
			try {
				PreparedStatement pst=con.prepareStatement(passportDetail);
				pst.setString(1,user);
				ResultSet rs=pst.executeQuery();  
				while(rs.next()){  
					usr.setPassportNumber(rs.getString(1));
//				System.out.println("password from db=="+rs.getString(1));  
				}  
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return usr;
			}
			String visaDetail="select visaNo from applyVisa where username=? and status='active'";					
			
			try {
				PreparedStatement pst=con.prepareStatement(visaDetail);
				pst.setString(1,user);
				ResultSet rs=pst.executeQuery();  
				while(rs.next()){  
					usr.setVisaNumber(rs.getString("visaNo"));
//				System.out.println("password from db=="+rs.getString(1));  
				}  
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return usr;
			}

			
			return usr;
			
		}
		
		return null;
	}
	
	
}
