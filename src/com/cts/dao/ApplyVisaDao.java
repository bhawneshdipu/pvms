package com.cts.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cts.beans.ApplyPassport;
import com.cts.beans.ApplyVisa;
import com.cts.utilities.DBUtility;

public class ApplyVisaDao {
	
	public ApplyVisa searchVisa(String visaNo,String visaType){
		Connection con=DBUtility.getConnection();
		ApplyVisa visa=null;
		String cmd="select * from applyVisa where visaNo=?";
		generateExpiryDate(visaNo,visaType);
		try {
			
			PreparedStatement pst=con.prepareStatement(cmd);
			pst.setString(1,visaNo);
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				visa=new ApplyVisa();
				visa.setPassportNo(rs.getString("passportNo"));
				visa.setUserName(rs.getString("userName"));
				visa.setCountry(rs.getString("country"));
				visa.setVisaType(rs.getString("visaType"));
				visa.setOccupation(rs.getString("occupation"));
				visa.setVisaNo(rs.getString("visaNo")); 
				visa.setIssueDate(rs.getDate("issueDate"));
				visa.setExpiryDate(rs.getDate("expiryDate"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return visa;
	}
	
	
	public String generateExpiryDate(String visaNo,String visaType){
		String cmd="";
		if(visaType.equals("Tourist"))
		{
			cmd="select DATE_ADD(curdate(), INTERVAL 6 MONTH) exp";
		}
		if(visaType.equals("Student"))
		{
			cmd="select DATE_ADD(curdate(), INTERVAL 2 YEAR) exp";
		}	
		if(visaType.equals("Business"))
		{
			cmd="select DATE_ADD(curdate(), INTERVAL 1 YEAR) exp";
		}
		if(visaType.equals("Transit"))
		{
			cmd="select DATE_ADD(curdate(), INTERVAL 5 DAY) exp";
		}
			
			Connection con=DBUtility.getConnection();
		String res="";
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(cmd);
			ResultSet rs=pst.executeQuery();
			rs.next(); 
			cmd="update applyVisa set expiryDate=? where visaNo=?";
			pst=con.prepareStatement(cmd); 
			pst.setDate(1,rs.getDate("exp")); 
			pst.setString(2,visaNo); 
			pst.executeUpdate();
			res="Expiry Date Created Successfully...";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res=e.getMessage();
		}
		return res;
		
	}
	
	public String generateVisaNo() {
		Connection con=DBUtility.getConnection();
		String cmdApplyPassport="select count(*) cnt from applyVisa";
		String visaNo="";
		PreparedStatement pst;
		ResultSet rs;
		try {
			pst=con.prepareStatement(cmdApplyPassport);
			rs=pst.executeQuery();
			rs.next();
			int cnt=rs.getInt("cnt");
			if(cnt==0){
				visaNo="VI2018I001";
			}
			else {
				cmdApplyPassport="select passportNo from applyPassport";
				pst=con.prepareStatement(cmdApplyPassport); 
				rs=pst.executeQuery();
				String pno="";
				while(rs.next()){
					pno=rs.getString("passportNo");
				}
				
				System.out.println("Passport NO " +pno);
				String part=pno.substring(7);
				System.out.println("Part is "+part);
				int no=Integer.parseInt(part);
				no++;
				if(no >= 1 && no <= 9){
					visaNo="VI2018I00"+no; 
				}
				if(no >= 10 && no <= 99){
					visaNo="VI2018I0"+no;
				}
				if(no >= 100 && no <= 999){
					visaNo="VI2018I"+no;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return visaNo ;
	}
	
	public boolean userExists(String user,String passportNo){
		boolean flag=false;
		String userCmd="select userName from applyPassport where userName=? and passportNo=?";
		Connection con=DBUtility.getConnection();
		try {
			PreparedStatement pst=con.prepareStatement(userCmd);
			pst.setString(1,user); 
			pst.setString(2,passportNo);
			
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				flag=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	

	public String applyVisa(ApplyVisa objApplyVisa) {
		
//		CustomerDao cd=new CustomerDao();
		boolean flag=userExists(objApplyVisa.getUserName(),objApplyVisa.getPassportNo());
		boolean flag2=userAlreadyApplied(objApplyVisa.getUserName());
		String userCmd="Insert into applyVisa(passportNo,userName,country,visaType,occupation,visaNo) values(?,?,?,?,?,?)";
		String msg="";
		if(flag == true && flag2 == false)
		{
			objApplyVisa.setVisaNo(generateVisaNo());
			Connection con=DBUtility.getConnection();
			   try {
					PreparedStatement pst=con.prepareStatement(userCmd);
					pst.setString(1,objApplyVisa.getPassportNo());
					pst.setString(2,objApplyVisa.getUserName());
					pst.setString(3,objApplyVisa.getCountry());
					pst.setString(4,objApplyVisa.getVisaType());
					pst.setString(5,objApplyVisa.getOccupation());
					pst.setString(6,objApplyVisa.getVisaNo());
				
				
					int n=pst.executeUpdate();
					if(n>0){
						msg="Successfully Applied for visa...";
						
					}
				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			      
			
		}
		else
		{
			msg="Already Applied";
		}
		
		
		return msg;
	}
	
	public boolean userAlreadyApplied(String user){
		boolean flag=false;
		//added
		String userCmd="select userName from applyVisa where userName=? and status='active'";
		Connection con=DBUtility.getConnection();
		try {
			PreparedStatement pst=con.prepareStatement(userCmd);
			pst.setString(1,user); 
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				flag=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	

}
