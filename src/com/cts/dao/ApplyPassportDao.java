package com.cts.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cts.beans.ApplyPassport;
import com.cts.utilities.DBUtility;

public class ApplyPassportDao {

	public ApplyPassport searchPassport(String passportNo){
		Connection con=DBUtility.getConnection();
		ApplyPassport passport=null;
		String cmd="select * from applyPassport where passportNo=?";
		//generateExpiryDate(passportNo);
		try {
			
			PreparedStatement pst=con.prepareStatement(cmd);
			pst.setString(1,passportNo);
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				passport=new ApplyPassport();
				passport.setPassportNo(rs.getString("passportNo"));
				passport.setUserName(rs.getString("userName"));				
				passport.setPassportOffice(rs.getString("passportOffice")); 
				passport.setBookletType(rs.getString("bookletType"));
				passport.setTypeOfService(rs.getString("typeOfService"));
				passport.setPassportNo(rs.getString("passportNo")); 
				passport.setIssueDate(rs.getDate("issueDate"));
				passport.setExpiryDate(rs.getDate("expiryDate"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return passport;
	}
		
	public String generatePassportNo() {
		Connection con=DBUtility.getConnection();
		String cmdApplyPassport="select count(*) cnt from applyPassport";
		String passportNo="";
		PreparedStatement pst;
		ResultSet rs;
		try {
			pst=con.prepareStatement(cmdApplyPassport);
			rs=pst.executeQuery();
			rs.next();
			int cnt=rs.getInt("cnt");
			if(cnt==0){
				passportNo="PS2018I001";
			}
			else {
				cmdApplyPassport="select passportNo from applyPassport";
				pst=con.prepareStatement(cmdApplyPassport); 
				rs=pst.executeQuery();
				String pno="";
				while(rs.next()){
					pno=rs.getString("passportNo");
				}
				
				System.out.println("Passport NO " +pno);
				String part=pno.substring(7);
				System.out.println("Part is "+part);
				int no=Integer.parseInt(part);
				no++;
				if(no >= 1 && no <= 9){
					passportNo="PS2018I00"+no; 
				}
				if(no >= 10 && no <= 99){
					passportNo="PS2018I0"+no;
				}
				if(no >= 100 && no <= 999){
					passportNo="PS2018I"+no;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return passportNo;
	}
	
	public String generateExpiryDate(String passportNo){
		String cmd="select DATE_ADD(curdate(), INTERVAL 5 YEAR) exp";
		Connection con=DBUtility.getConnection();
		String res="";
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(cmd);
			ResultSet rs=pst.executeQuery();
			rs.next(); 
			cmd="update applyPassport set expiryDate=? where passportNo=?";
			pst=con.prepareStatement(cmd); 
			pst.setDate(1,rs.getDate("exp")); 
			pst.setString(2,passportNo); 
			pst.executeUpdate();
			res="Expiry Date Created Successfully...";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res=e.getMessage();
		}
		return res;
		
	}
	public String applyPassport(ApplyPassport objApplyPassport)
	{
		CustomerDao cd=new CustomerDao();
//		System.out.println("db="+objApplyPassport.getUserName());
		boolean flag=cd.userExists(objApplyPassport.getUserName());
		
		boolean flag2=userExists(objApplyPassport.getUserName());
		String msg="";
		if(flag==true  && flag2==false){
			Connection con=DBUtility.getConnection();
			objApplyPassport.setPassportNo(generatePassportNo());
			String cmdApplyPassIns="insert into applyPassport(userName,passportOffice,typeOfService,bookletType,passportNo) values(?,?,?,?,?)";
	      try {
			PreparedStatement pst=con.prepareStatement(cmdApplyPassIns);
			pst.setString(1,objApplyPassport.getUserName());			
			pst.setString(2,objApplyPassport.getPassportOffice());
			pst.setString(3,objApplyPassport.getTypeOfService());
			pst.setString(4,objApplyPassport.getBookletType());
			pst.setString(5,objApplyPassport.getPassportNo());
			int n=pst.executeUpdate();
			if(n>0){
				msg="Successfully Applied...";
				
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      
		}
		else {
			if(flag == false)
			{
			msg="Invalid UserName";
			}
			else if(flag2==true)
			{
				msg="User already applied for passport";
			}
		}
		return msg;
		
		
	}
	public boolean userExists(String user){
		boolean flag=false;
		String userCmd="select userName from applyPassport where userName=?";
		Connection con=DBUtility.getConnection();
		try {
			PreparedStatement pst=con.prepareStatement(userCmd);
			pst.setString(1,user); 
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				flag=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	public ApplyPassport renewalPassportData(String userName) {
		ApplyPassport objApplyPassport=new ApplyPassport();
		String userCmd="select passportNo,issueDate,expiryDate from applyPassport where userName=?";
		Connection con=DBUtility.getConnection();
		try {
			PreparedStatement pst=con.prepareStatement(userCmd);
			pst.setString(1,userName); 
			
			ResultSet rs=pst.executeQuery();
		if(rs.next())
		{
			objApplyPassport.setPassportNo(rs.getString(1));
			objApplyPassport.setIssueDate(rs.getDate(2));
			objApplyPassport.setExpiryDate(rs.getDate(3));
		}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return objApplyPassport;
	}

//	public void generateRenewalssueDate(String passportNo, java.sql.Date issueDate) {
//		String userCmd="Update applyPassport set issueDate=?  where passportNo=?";
//		Connection con=DBUtility.getConnection();
//		PreparedStatement pst=con.prepareStatement(userCmd);
//		pst.setDate(1,issueDate); 
//		
//		ResultSet rs=pst.executeQuery();
//		
//	}
	
	
	
	
}
