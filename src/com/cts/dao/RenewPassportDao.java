package com.cts.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cts.beans.ApplyPassport;
import com.cts.utilities.DBUtility;

public class RenewPassportDao {
	
	public ApplyPassport checkLostPassPort(String passportNo){
		Connection con=DBUtility.getConnection();
		ApplyPassportDao dao=new ApplyPassportDao(); 
		ApplyPassport obj=dao.searchPassport(passportNo);
		
		try {			
			String cmd="Insert into renewPassport(passportNo,RenewalReason) values(?,?)";
			PreparedStatement pst=con.prepareStatement(cmd);			
			
			pst.setString(1,passportNo);
			pst.setString(2,"Passport Lost");
			pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	
	public ApplyPassport checkDamagedPassport(String passportNo){
		Connection con=DBUtility.getConnection();
		ApplyPassportDao dao=new ApplyPassportDao(); 
		ApplyPassport obj=dao.searchPassport(passportNo);
		
		try {			
			String cmd="Insert into renewPassport(passportNo,RenewalReason) values(?,?)";
			PreparedStatement pst=con.prepareStatement(cmd);			
			
			pst.setString(1,passportNo);
			pst.setString(2,"Passport Damaged");
			pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	
	public ApplyPassport renewPassportExpiry(String passportNo) {
		Connection con=DBUtility.getConnection();
		ApplyPassportDao dao=new ApplyPassportDao(); 
		ApplyPassport obj=dao.searchPassport(passportNo);
		
		//System.out.println(obj.getExpiryDate());
	//	boolean status=false;
		if(obj!=null){
			try {
				PreparedStatement pst=con.prepareStatement("select datediff(curDate(),?) dif");
				
				pst.setDate(1,obj.getExpiryDate());
				ResultSet rs=pst.executeQuery();
				rs.next();
				int dif=rs.getInt("dif");
				System.out.println(dif);
				if(dif < 0){
					//System.out.println("Still Your Passport is Active...");
					obj =null;
				}
				else {
					dao.generateExpiryDate(passportNo);
				
					String cmd="Insert into renewPassport(passportNo,RenewalReason) values(?,?)";
					pst=con.prepareStatement(cmd); 
					pst.setString(1,passportNo);
					pst.setString(2,"Passport Expired");
					pst.executeUpdate();
				//	System.out.println("Passport Renewed Successfully...");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		return obj;	
	}	

}
