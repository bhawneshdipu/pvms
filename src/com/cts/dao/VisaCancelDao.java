package com.cts.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cts.beans.ApplyVisa;
import com.cts.utilities.DBUtility;

public class VisaCancelDao {

	public String visaExpire(String visaNo){
		boolean flag=checkVisaStatus(visaNo);  
		String msg="";
		if(flag==false)
		{
			msg="Visa Already cancelled";
		}
		else
		{
		ApplyVisa objVisa=searchVisa(visaNo);
		Connection con=DBUtility.getConnection();
		
		if(objVisa!=null){
			Date dt=objVisa.getExpiryDate();
			String cmd="select datediff(curDate(),?) dif";
			try {
				PreparedStatement pst=con.prepareStatement(cmd);
				pst.setDate(1,dt);
				ResultSet rs=pst.executeQuery();
				rs.next();
				int dif=rs.getInt("dif");
				System.out.println(dif);
				if(dif > 0){
					msg="Already Expired";
				}
				else {
					cmd="update applyVisa set status='Inactive' where VisaNo=?";
					pst=con.prepareStatement(cmd);
					pst.setString(1, visaNo);
					pst.executeUpdate();
					msg="Visa Successfully Cancelled";
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		else {
			msg="Invalid VisaNo...";
		}
		}
		return msg;
	}
	
	public ApplyVisa searchVisa(String visaNo){
		Connection con=DBUtility.getConnection();
		ApplyVisa objVisa=null;
		
		
		String cmd="select * from applyVisa where visaNo=?";
		try {
			PreparedStatement pst=con.prepareStatement(cmd);
			pst.setString(1,visaNo);
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				objVisa=new ApplyVisa();
				objVisa.setVisaNo(rs.getString("visano"));
				objVisa.setPassportNo(rs.getString("passportNo"));
				objVisa.setUserName(rs.getString("userName"));
				objVisa.setCountry(rs.getString("country"));
				objVisa.setVisaType(rs.getString("visaType"));
				objVisa.setOccupation(rs.getString("occupation"));
				objVisa.setIssueDate(rs.getDate("issueDate"));
				objVisa.setExpiryDate(rs.getDate("expiryDate"));
				objVisa.setStatus(rs.getString("status"));
			}
			else {
				objVisa=null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objVisa;
	}

	public boolean checkVisaStatus(String visaNo) {
		boolean flag=false;
		Connection con=DBUtility.getConnection();
		String cmd="select status from applyVisa where visaNo=?";
		String status="";
		try {
			PreparedStatement pst=con.prepareStatement(cmd);
			pst.setString(1,visaNo);
			ResultSet rs=pst.executeQuery();
			if(rs.next()) {
				status=rs.getString(1);	
			}else {
				return false;
			}
			System.out.println(status);
			if(status.equals("Inactive"))
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
}